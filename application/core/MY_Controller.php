<?php

use Philo\Blade\Blade;

/**
 * Modified core Controller with Blade Templating
 * 
 * @package	rbm.skeleton
 * @subpackage  core
 * @author	Main Developer: Rodrigo Borges <https://rbm.dev.br>
 * @copyright	Copyright (c) 2020, Rodrigo Borges. <https://rbm.dev.br>
 * @license	MIT
 * @link	http://skeleton.myara.net
 * @since	Version master
 * @filesource
 */
class MY_Controller extends CI_Controller 
{
    protected $data;
    protected $blade;
    protected $bladeViewPath;
    protected $bladeCachePath;

    public function __construct($withBlade = true)
    {
        parent::__construct();

        if ($withBlade) {
            $this->bladeViewPath = APPPATH . 'views';
            $this->bladeCachePath = APPPATH . 'cache';
            $this->blade = new Blade($this->bladeViewPath, $this->bladeCachePath);
        }
    }

    /**
     * Render a View on Blade Templating
     * 
     * @param string $view The View located at application/views/
     * @param array $data (Optional) An associative array of data 
     *                               to be extracted for use in the view
     *                               if the $data is not specified,
     *                               the view will use $this->data
     * @param bool $return (Optional) Return view string instead of echo
     */
    public function bladeView($view, $data = array(), $return = false)
    {
        if ($data) {
            $this->data = array_merge($this->data, $data);
        }
        
        $view = $this->blade->view()->make($view, $this->data)->render();

        if ($return) {
            return $view;
        }

        echo $view;
    }
}

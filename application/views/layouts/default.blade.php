{{--
/**
 * Default Layout
 *
 * @package	rbm.skeleton
 * @subpackage  view
 * @author	Rodrigo Borges <https://rbm.dev.br>
 * @copyright	Copyright (c) 2020, Rodrigo Borges <https://rbm.dev.br>
 * @license	MIT
 * @link	http://skeleton.myara.net
 * @since	Version master
 * @filesource
 */
--}}

@include ('layouts.includes.header')

@include ('layouts.menus.default')

    <div class="container-fluid">
        <h1>Hello, world!</h1>
        @yield ('content')
    </div>

@include ('layouts.includes.footer')

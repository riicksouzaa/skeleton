{{--
/**
 * Welcome view example
 *
 * @package	rbm.skeleton
 * @subpackage  view
 * @author	Rodrigo Borges <https://rbm.dev.br>
 * @copyright   Rodrigo Borges <https://rbm.dev.br>
 * @license	MIT
 * @link	http://skeleton.myara.net
 * @since	Version master
 * @filesource
 */
--}}

@extends ('layouts.default')

@section ('content')

    <div class="row">
        <div class="col-md-12">
            <h3>Welcome</h3>
            <p>This is the content of the page</p>
        </div>
    </div>

@endsection

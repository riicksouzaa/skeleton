# CodeIgniter Skeleton

[![pipeline status](https://gitlab.com/rbm/skeleton/badges/master/pipeline.svg)](https://gitlab.com/rbm/skeleton/-/commits/master)
[![coverage report](https://gitlab.com/rbm/skeleton/badges/master/coverage.svg)](https://gitlab.com/rbm/skeleton/-/commits/master)

CodeIgniter start project with:
* [CodeIgniter base via composer](https://gitlab.com/rbm/codeigniter-base)
* [Blade Templating](https://packagist.org/packages/philo/laravel-blade)
* Separate public folder for better security
* Template with [Bootstrap 4](http://getbootstrap.com)


## How to Install
* clone the repo with `git clone https://gitlab.com/rbm/skeleton.git`
* execute `php composer install` (if you don't have composer [install first](https://getcomposer.org/download/))
* create vhosts in **public folder**
* if you want to run without apache or nginx just for testing purposes you can use bultin php webserver:
```console
$ cd public && php -S localhost:8080
```
and access the project in http://localhost:8080

## License
[MIT License](LICENSE)

## Credits

### Skeleton
* Rodrigo Borges <https://rbm.dev.br>

### Third-party depedencies
* [CodeIgniter](https://github.com/bcit-ci/CodeIgniter)
* [Philo Laravel Blade](https://packagist.org/packages/philo/laravel-blade)
* [Twitter Bootstrap](http://getbootstrap.com/)


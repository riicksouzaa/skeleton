<?php

use PHPUnit\Framework\TestCase;
use Philo\Blade\Blade;

/**
 * Temporary Controller to get Blade object from MY_Controller
 */  
class BladeTestInstance extends MY_Controller
{
    public function getBladeObject()
    {
        return $this->blade;
    }

    public function getBladePaths()
    {
        return [$this->bladeViewPath, $this->bladeCachePath];
    }
}

/**
 * Test Case for Blade Templating
 */
class BladeTemplatingTest extends TestCase
{
    public function testBladeInstance()
    {
        $bladeTest = new BladeTestInstance();
        $bladePaths = $bladeTest->getBladePaths();
        $this->assertEquals($bladeTest->getBladeObject(), new Blade($bladePaths[0], $bladePaths[1]));
    }
}

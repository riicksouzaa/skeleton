<?php

use PHPUnit\Framework\TestCase;

/**
 * Test Case for Welcome Controller and Controllers in general
 */ 
class WelcomeTest extends TestCase
{
    public function __construct()
    {
        parent::__construct();
        $this->controller = new Welcome();

    }
    public function testIndex()
    {
        $this->controller->index();
        $this->expectOutputRegex('/This is the content of the page/');
    }

}
